package annotations;


public @interface Test {
    Class<? extends Throwable> expected() default None.class;
    String name() default "";

    /**
     * Default empty exception.
     */
    static class None extends Throwable {
        private None() {
        }
    }
}
