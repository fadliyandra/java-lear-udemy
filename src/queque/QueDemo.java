package queque;

import java.util.PriorityQueue;
import java.util.Queue;

public class QueDemo {
    public static void main(String[] args) {
        Queue<Integer> queue = new PriorityQueue<>();

        queue.offer(2);
        queue.offer(8);
        queue.offer(3);
        queue.offer(1);
        queue.offer(10);
        System.out.println("get head of the que and remove element : " + queue.poll());

    }
}
