package map;

import java.util.LinkedHashMap;
import java.util.Map;

public class LinkedHasMapdemo extends LinkedHashMap<Integer, String> {
    private int capacity = 3;

    @Override
    public boolean removeEldestEntry(Map.Entry<Integer, String> eldest) {
        if (size() > this.capacity) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        LinkedHasMapdemo  map = new  LinkedHasMapdemo ();

        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");

        System.out.println(map);
    }
}
