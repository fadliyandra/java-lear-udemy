package map;

import java.util.NavigableMap;
import java.util.TreeMap;

public class TreeMapDemo {
    public static void main(String[] args) {
        NavigableMap<Integer, String> treeMap = new TreeMap<>();
        treeMap.put(1,"one");
        treeMap.put(5,"five");
        treeMap.put(2,"two");
        treeMap.put(4,"four");
        treeMap.put(3,"three");

        System.out.println(treeMap);
        System.out.println(treeMap.descendingMap());
        System.out.println(treeMap.firstEntry());
        System.out.println(treeMap.floorEntry(2));
        System.out.println(treeMap.higherEntry(2));
        System.out.println(treeMap.ceilingEntry(3));

        NavigableMap<Product, User> productUserMap =
                new TreeMap<>(new CustomProductComparator());
        productUserMap.put(new DefaultProduct(2, "Oregon Cottage Interior Oak Door", "Doors", 109.98), new DefaultUser());
        productUserMap.put(new DefaultProduct(1, "Hardwood Oak Suffolk Internal Door", "Doors", 109.98), new DefaultUser());

        System.out.println("***** Demo - Keys are sorted according to Comparator: ");
        for (Product product : productUserMap.keySet()) {
            System.out.println(product);
        }


    }
}
