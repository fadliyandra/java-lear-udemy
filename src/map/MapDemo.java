package map;

import java.util.HashMap;
import java.util.Map;

public class MapDemo {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(2,"two");
        map.put(1, "one");
        map.put(3, "three");
        map.put(4,null);

        //get element
        System.out.println(map.get(1));

        //iterate over map keys
        System.out.println();
        System.out.println("Iterating over map keys demo : ");
        for (Integer key : map.keySet()){
            System.out.println(key);
        }

        //iterate over map entries
        System.out.println("interating over map entries demo :");
        for (Map.Entry<Integer, String> entry : map.entrySet()){
            System.out.println(entry.getKey() + entry.getValue());
        }

        //getOrderDevault Demo
        System.out.println(map.getOrDefault(4,"default"));
        System.out.println(map.getOrDefault(5,"default"));

        //putAbsenDemo
        map.putIfAbsent(4, "four");
        System.out.println(map.get(4));

        //Has tables demo
        Map<User, Product> userProductMap = new HashMap<>();
        User user = new DefaultUser(1, "John", "Smith", "password", "john.smith@email.com");
        Product product = new DefaultProduct(1, "product name 1", "product category 1",
                99.99);
        userProductMap.put(user, product);

        User userCopy = new DefaultUser(1, "John", "Smith", "password",
                "john.smith@email.com");

        System.out.println("get product by user from map " + userProductMap.get(userCopy));

    }


}
