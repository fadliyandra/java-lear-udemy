package refflection;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class Demo {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchFieldException{

        Class userClass = new User().getClass();
        System.out.println("Get class name: " + userClass.getName());

        Field[] fields = userClass.getDeclaredFields();
        System.out.println("==== get field names ===");
        Arrays.stream(fields).forEach(field -> System.out.println(field.getName()));

        System.out.println("====get field tipe====");
        Arrays.stream(fields).forEach(field -> System.out.println(field.getGenericType()));

        System.out.println("======does field have private modifier =====");
        Arrays.stream(fields).forEach(field -> System.out.println(Modifier.isPrivate(field.getModifiers())));

        System.out.println("Get package name: " + userClass.getPackageName());

        System.out.println("Get super class: " + userClass.getSuperclass());

        Class[] interfaces = userClass.getInterfaces();
        Arrays.stream(interfaces).forEach(i -> System.out.println(i.getName()));

        System.out.println("===== Get Constructors =====");
        Constructor<User>[] constructors = userClass.getDeclaredConstructors();
        System.out.println("Number of constructors: " + constructors.length);
        Arrays.stream(constructors).forEach(constructor -> System.out.println(constructor.getName()));
    }

    private static class User implements Serializable, Cloneable {
        private static int id = 0;

        private String firstName;
        private String lastName;
        private String email;

        static {
            id++;
        }

        public User() {
            this("default@user.com");
        }

        private User(String defaultEmail) {
            this.email = defaultEmail;
        }

        public void printFullNameToConsole() {
            System.out.println(firstName + " " + lastName);
        }

        public double calculateTotalAmountSpentInOnlineStore() {
            return 1125.65; // some dummy value
        }

        public void mergeTwoUserAccounts(User anotherUser) {
            // merging order history and other data of two accounts
        }

        private void doSomething(String someString) {
            System.out.println("Private method invocation with argument: " + someString);
        }

        private static void doSomethingStatic(String someString) {
            System.out.println("Private static method invocation with argument: " + someString);
        }

        @Override
        public String toString() {
            return "User [id=" + id +", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + "]";
        }


    }
}
