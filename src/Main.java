import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Integer> integers = new ArrayList<>(Arrays.asList(1,2,3,4,5,8));

        System.out.println("=====itertor demo ========");

        Iterator<Integer> iterator = integers.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println("iterator remove demo");
        iterator = integers.iterator();
        while(iterator.hasNext()){
            int nextInt = iterator.next();
            if (nextInt %2 ==0){
                iterator.remove();
            }
        }
        System.out.println(integers);

    }
}