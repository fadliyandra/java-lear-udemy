package lamdamethodreference;

public class LambdaAndMethodReferenceDemo {
    public static void main(String[] args) {
    //lamda functio demo
        OrderManagement orderManagement=  new OrderManagement(new DefaultDistanceCalculator());
        orderManagement.setDistanceCalculator(new DefaultDistanceCalculator(){
            @Override
            public double calculateDistance(City city1, City city2) {
                return city1.getLatitude() - city2.getLatitude();
            }
        });
        DistanceCalculator dCalculator = (city1, city2) -> city1.getLogitude() - city2.getLogitude();
        DistanceCalculator dCalculator2 = (city1, city2) -> {
            System.out.println("Text inside lambda function");
            return city1.getLogitude() - city2.getLogitude();
        };

        dCalculator2.calculateDistance(new City(), new City());

        orderManagement.setDistanceCalculator((city1, city2) -> city1.getLogitude() - city2.getLogitude());

        orderManagement
                .setDistanceCalculator(GoogleDistanceCalculator::getDistanceBetweenCitiesStatic);

        GoogleDistanceCalculator gdc = new GoogleDistanceCalculator();
        orderManagement
                .setDistanceCalculator((city1, city2) -> gdc.getDistanceBetweenCities(city1, city2));
    }
}

class OrderManagement{
    private DistanceCalculator distanceCalculator;

    public OrderManagement(DistanceCalculator distanceCalculator) {
        this.distanceCalculator = distanceCalculator;
    }

    public void setDistanceCalculator(DistanceCalculator distanceCalculator) {
        this.distanceCalculator = distanceCalculator;
    }
}

class DefaultDistanceCalculator implements  DistanceCalculator{
    @Override
    public double calculateDistance(City city1, City City2) {
        return 0;
    }
}

class GoogleDistanceCalculator{
    public double getDistanceBetweenCities(City city1, City city2) {
        return 1;
    }

    public static double getDistanceBetweenCitiesStatic(City city1, City city2) {
        return 1;
    }
}


